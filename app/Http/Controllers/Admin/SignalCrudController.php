<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\SignalRequest;
use App\Models\Signal;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
use Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
use Backpack\CRUD\app\Http\Controllers\Operations\InlineCreateOperation;
use Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
use Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;
use Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanel;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class SignalCrudController
 * @package App\Http\Controllers\Admin
 * @property-read CrudPanel $crud
 */
class SignalCrudController extends CrudController
{
    use ListOperation;
    use CreateOperation;
    use UpdateOperation;
    use DeleteOperation;
    use ShowOperation;
    use InlineCreateOperation;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     *
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(Signal::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/signal');
        CRUD::setEntityNameStrings('signal', 'signals');

        $this->crud->orderBy('id','DESC');

    }

    /**
     * Define what happens when the List operation is loaded.
     *
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
        CRUD::addColumns([

            [   // DateTime
                'name' => 'date_time',
                'label' => 'Date/Time',
                'type' => 'datetime',
                'format' => 'DD.MM.YYYY / HH:mm',
            ],
            [
                'name' => 'symbol',
                'type' => 'text',
                'label' => 'Symbol'
            ],
            [
                'name' => 'order',
                'type' => 'enum',
                'label' => 'Order'
            ],
            [
                'name' => 'price',
                'type' => 'text',
                'label' => 'Sell/ Buy price'
            ],
            [
                'name' => 'stop_loss',
                'type' => 'text',
                'label' => 'SL'
            ],
            [
                'name' => 'take_profit',
                'type' => 'text',
                'label' => 'TP'
            ],
            [
                'name' => 'paid_subscription',
                'type' => 'check',
            ],
        ]);

        /**
         * Columns can be defined using the fluent syntax or array syntax:
         * - CRUD::column('price')->type('number');
         * - CRUD::addColumn(['name' => 'price', 'type' => 'number']);
         */
    }

    /**
     * Define what happens when the Create operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        CRUD::setValidation(SignalRequest::class);

        CRUD::addFields([

            [
                'name' => 'symbol',
                'type' => 'text',
                'label' => 'Symbol'
            ],
            [
                'name' => 'order',
                'type' => 'enum',
                'label' => 'Order'
            ],
            [
                'name' => 'price',
                'type' => 'text',
                'label' => 'Sell/ Buy price'
            ],
            [
                'name' => 'stop_loss',
                'type' => 'text',
                'label' => 'SL'
            ],
            [
                'name' => 'take_profit',
                'type' => 'text',
                'label' => 'TP'
            ],
            [   // DateTime
                'name' => 'date_time',
                'label' => 'Datetime',
                'type' => 'datetime_picker',

                // optional:
                'datetime_picker_options' => [
                    'format' => 'DD.MM.YYYY / HH:mm',
                    'language' => 'en'
                ],
                'allows_null' => true,
                // 'default' => '2017-05-12 11:59:59',
            ],
            [  // Select2
                'label' => "Type of instrument",
                'type' => 'relationship',
                'name' => 'instrument_id', // the db column for the foreign key
                'entity' => 'instrument', // the method that defines the relationship in your Model
                'attribute' => 'title', // foreign key attribute that is shown to user
            ],
            [
                'label' => "Type of sector",
                'type' => 'relationship',
                'name' => 'sector_id', // the db column for the foreign key
                'entity' => 'sector', // the method that defines the relationship in your Model
                'attribute' => 'title', // foreign key attribute that is shown to user
            ],
            [
                'type' => 'ckeditor',
                'name' => 'detailed_signal_info',
            ],
            [
                'type' => 'checkbox',
                'name' => 'paid_subscription',
                'default'=>1,
            ],
        ]);

        /**
         * Fields can be defined using the fluent syntax or array syntax:
         * - CRUD::field('price')->type('number');
         * - CRUD::addField(['name' => 'price', 'type' => 'number']));
         */
    }

    /**
     * Define what happens when the Update operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }

    protected function setupShowOperation()
    {
        $this->crud->removeColumn('instrument_id');
        $this->crud->removeColumn('sector_id');
        CRUD::addColumns([

            [
                'name' => 'symbol',
                'type' => 'text',
                'label' => 'Symbol'
            ],
            [
                'name' => 'price',
                'type' => 'text',
                'label' => 'Price'
            ],
            [
                'name' => 'order',
                'type' => 'enum',
                'label' => 'Order'
            ],
            [
                'name' => 'stop_loss',
                'type' => 'text',
                'label' => 'SL'
            ],
            [
                'name' => 'take_profit',
                'type' => 'text',
                'label' => 'TP'
            ],
            [   // DateTime
                'name' => 'date_time',
                'label' => 'Datetime',
                'type' => 'datetime_picker',
                'format' => 'DD.MM.YYYY / HH:mm',

            ],
            [  // Select2
                'label' => "Type of instrument",
                'type' => 'select',
                'name' => 'instrument_id', // the db column for the foreign key
                'entity' => 'instrument', // the method that defines the relationship in your Model
                'attribute' => 'title', // foreign key attribute that is shown to user
            ],
            [
                'label' => "Type of sector",
                'type' => 'select',
                'name' => 'sector_id', // the db column for the foreign key
                'entity' => 'sector', // the method that defines the relationship in your Model
                'attribute' => 'title', // foreign key attribute that is shown to user
            ],
//            [
//                'type' => 'text',
//                'name' => 'detailed_signal_info',
//            ],
            [
                'type' => 'check',
                'name' => 'paid_subscription',
            ],
        ]);

    }
}
