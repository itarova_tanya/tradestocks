<!-- This file is used to store sidebar items, starting with Backpack\Base 0.9.0 -->
<li class="nav-item"><a class="nav-link" href="{{ backpack_url('dashboard') }}"><i class="la la-home nav-icon"></i> {{ trans('backpack::base.dashboard') }}</a></li>
<li class='nav-item'><a class='nav-link' href='{{ backpack_url('sector') }}'><i class='nav-icon la la-question'></i> Types of sector</a></li>
<li class='nav-item'><a class='nav-link' href='{{ backpack_url('instrument') }}'><i class='nav-icon la la-question'></i> Types of instrument</a></li>
<li class='nav-item'><a class='nav-link' href='{{ backpack_url('signal') }}'><i class='nav-icon la la-question'></i> Signals</a></li>
<li class='nav-item'><a class='nav-link' href='{{ backpack_url('language') }}'><i class='nav-icon la la-question'></i> Languages</a></li>
<li class='nav-item'><a class='nav-link' href='{{ backpack_url('termsconditions') }}'><i class='nav-icon la la-question'></i> Terms&Conditions</a></li>

<li class='nav-item'><a class='nav-link' href='{{ backpack_url('subscription') }}'><i class='nav-icon la la-question'></i> Subscriptions</a></li>
