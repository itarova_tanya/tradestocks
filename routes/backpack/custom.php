<?php

// --------------------------
// Custom Backpack Routes
// --------------------------
// This route file is loaded automatically by Backpack\Base.
// Routes you generate using Backpack\Generators will be placed here.

Route::group([
    'prefix'     => config('backpack.base.route_prefix', 'admin'),
    'middleware' => [
        config('backpack.base.web_middleware', 'web'),
        config('backpack.base.middleware_key', 'admin'),
    ],
    'namespace'  => 'App\Http\Controllers\Admin',
], function () { // custom admin routes
    Route::crud('sector', 'SectorCrudController',['except'=>['delete']]);
    Route::get('sector/{id}/delete', 'SectorCrudController@destroy');

    Route::crud('instrument', 'InstrumentCrudController',['except'=>['destroy']]);
    Route::get('instrument/{id}/delete', 'InstrumentCrudController@destroy');

    Route::crud('signal', 'SignalCrudController',['except'=>['delete']]);
    Route::get('signal/{id}/delete', 'SignalCrudController@destroy');

    Route::crud('role', 'RoleCrudController');

    Route::crud('language', 'LanguageCrudController',['except'=>['delete']]);
    Route::get('language/{id}/delete', 'LanguageCrudController@destroy');

    Route::crud('termsconditions', 'TermsConditionsCrudController',['except'=>['delete']]);
    Route::get('termsconditions/{id}/delete', 'TermsConditionsCrudController@destroy');

    Route::crud('subscription', 'SubscriptionCrudController');
    Route::get('subscription/{id}/delete', 'SubscriptionCrudController@destroy');
}); // this should be the absolute last line of this file
